<?php

namespace App\Tests;

use App\Entity\Location;
use App\Entity\Client;
use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{
    public function testLocation()
    {
        $location = new Location();

        $location->setDateDebut(new \DateTime('2023-01-01'));
        $location->setDateRetour(new \DateTime('2023-01-10'));
        $location->setPrix(500);

        $this->assertEquals(new \DateTime('2023-01-01'), $location->getDateDebut());
        $this->assertEquals(new \DateTime('2023-01-10'), $location->getDateRetour());
        $this->assertEquals(500, $location->getPrix());

        $client = new Client();
        $voiture = new Voiture();

        $location->setClient($client);
        $location->setVoiture($voiture);

        $this->assertInstanceOf(Client::class, $location->getClient());
        $this->assertInstanceOf(Voiture::class, $location->getVoiture());
    }
}







